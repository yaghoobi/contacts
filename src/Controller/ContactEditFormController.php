<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contacts;
use App\Repository\ContactsRepository;

# Form
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use App\Form\Type\ContactType;

# Doctrine
use Doctrine\Persistence\ManagerRegistry;

class ContactEditFormController extends AbstractController
{
	public function update(Request $request, ManagerRegistry $doctrine)
	{
		$entityManager = $doctrine->getManager();
		$itemRecordId = $request->get('id');

		if($itemRecordId == 0){
			return new Response('For Edition Contact id is needed ===> /contact_edit_form/id');
		} else {
			$item = $doctrine
				->getRepository(Contacts::class)
				->find($itemRecordId );
		}

		$form = $this->createForm(ContactType::class, $item );

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$contact = $form->getData();
			$entityManager->persist($contact);

			// actually executes the queries (i.e. the UPDATE query)
			$entityManager->flush();

			//return new Response('Item updated!');
			return $this->redirectToRoute('contacts');
		}
		
		return $this->renderForm('contacts/form.html.twig', [
			'form'=>$form
		]);
	}
}
