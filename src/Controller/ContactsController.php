<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contacts;
use App\Repository\ContactsRepository;
use Doctrine\Persistence\ManagerRegistry;

class ContactsController extends AbstractController
{
    public function index(ManagerRegistry $doctrine): Response
    {
		$repository = $doctrine->getRepository(Contacts::class);
		$contacts = $repository->findAll();
		
		$data = $this->prepare_table_data($contacts);

        return $this->render('contacts/index.html.twig', [
			'contacts'=>$data,
		]);
    }
    private function prepare_table_data(array $contacts): array
    {
		$titles = [
			'id' => 'Id',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'street_and_number' => 'Street and Number',
			'zip' => 'Zip',
			'city' => 'City',
			'country' => 'Country',
			'phone_number' => 'Phone Number',
			'birth_day' => 'Birth Day',
			'email_address' => 'E-mail Address',
		];
		$data = array($titles);
		foreach($contacts as $contact) {
			if( is_null($contact->getBirthDay()) ) {
				$birthDay = '';
			} else {
				$birthDay = $contact->getBirthDay()->format('Y/m/d');
			}
			array_push($data,[
				'id' => $contact->getId(),
				'first_name' => $contact->getFirstName(),
				'last_name' => $contact->getLastName(),
				'street_and_number' => $contact->getStreetAndNumber(),
				'zip' => $contact->getZip(),
				'city' => $contact->getCity(),
				'country' => $contact->getCountry(),
				'phone_number' => $contact->getPhoneNumber(),
				'birth_day' => $birthDay,
				'email_address' => $contact->getEmailAddress(),
			]);			
		}
		return $data;
	}
}
