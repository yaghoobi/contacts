<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contacts;
use App\Repository\ContactsRepository;

# Form
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use App\Form\Type\ContactType;

# Doctrine
use Doctrine\Persistence\ManagerRegistry;

# Validator
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContactFormController extends AbstractController
{
    public function index(Request $request, ManagerRegistry $doctrine, ValidatorInterface $validator): Response
    {
		$entityManager = $doctrine->getManager();
		
		// Creating FORM
		$contact = new Contacts();
		$form = $this->createForm(ContactType::class, $contact);

		$form->handleRequest($request);

        $errors = $validator->validate($contact);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }

		if ($form->isSubmitted() && $form->isValid()) {
			$contact = $form->getData();

			$entityManager->persist($contact);

			// actually executes the queries (i.e. the INSERT query)
			$entityManager->flush();

			//return new Response('Saved new contact with id '.$contact->getId());
			return $this->redirectToRoute('contacts');
		}

		return $this->renderForm('contacts/form.html.twig', [
			'form'=>$form
		]);
    }
}
