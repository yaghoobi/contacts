<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Contacts;
use App\Repository\ContactsRepository;

# Form
use Symfony\Component\HttpFoundation\Request;
use App\Form\Type\ContactType;

# Doctrine
use Doctrine\Persistence\ManagerRegistry;

class ContactDeleteFormController extends AbstractController
{
	public function delete_record(Request $request, ManagerRegistry $doctrine)
	{
		$entityManager = $doctrine->getManager();
		$itemRecordId = $request->get('id');

		if($itemRecordId == 0){
			return new Response('For Deleting a Contact, its id is needed ===> /contact_delte_form/id');
		} else {
			$item = $doctrine
				->getRepository(Contacts::class)
				->find($itemRecordId );
		}

		$entityManager->remove($item);
		$entityManager->flush();

		//return new Response('Item deleted!');
		return $this->redirectToRoute('contacts');
	}
}
