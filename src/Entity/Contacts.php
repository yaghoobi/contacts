<?php

namespace App\Entity;

use App\Repository\ContactsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ContactsRepository::class)]
class Contacts
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 30)]
    private $first_name;

    #[ORM\Column(type: 'string', length: 30)]
    private $last_name;

    #[ORM\Column(type: 'string', length: 50, nullable: true)]
    private $street_and_number;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    private $zip;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    private $city;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    private $country;

    #[ORM\Column(type: 'string', length: 30)]
    private $phone_number;

    #[ORM\Column(type: 'date', nullable: true)]
    private $birth_day;

    #[ORM\Column(type: 'string', length: 30, nullable: true)]
    private $email_address;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getStreetAndNumber(): ?string
    {
        return $this->street_and_number;
    }

    public function setStreetAndNumber(?string $street_and_number): self
    {
        $this->street_and_number = $street_and_number;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getBirthDay(): ?\DateTimeInterface
    {
        return $this->birth_day;
    }

    public function setBirthDay(?\DateTimeInterface $birth_day): self
    {
        $this->birth_day = $birth_day;

        return $this;
    }

    public function getEmailAddress(): ?string
    {
        return $this->email_address;
    }

    public function setEmailAddress(?string $email_address): self
    {
        $this->email_address = $email_address;

        return $this;
    }
}
